﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMSBDlibrary;

namespace WMS_service.ModelBinder
{
    public class WMSLayerBinder:DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return base.BindModel(controllerContext, bindingContext);
        }
        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
          WMSLayer model=(WMSLayer) bindingContext.Model;
          if (model.Name == null)
              bindingContext.ModelState.AddModelError("Name", "Невозможно пустое значение");
            base.OnModelUpdated(controllerContext, bindingContext);
        }
    }
}