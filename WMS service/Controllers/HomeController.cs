﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS_service.Models;
using WMSBDlibrary;

namespace WMS_service.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
          IRepository repository;
          IGenericXmlWms generic;
          public HomeController(IRepository repository,IGenericXmlWms generic)
        {
            this.repository = repository;
            this.generic = generic;
        }
          public ActionResult RemoveLayer(string name)
          {
              repository.RemoveLayer(name);
              return RedirectToAction("Index");
          }
          public ActionResult Detail(WMSLayer SelectedLayer)
          {
              
              if (TempData["ModelState"] != null)
                  ViewData = (ViewDataDictionary)TempData["ModelState"];
              if (SelectedLayer == null)
              {
                  SelectedLayer = new WMSLayer();
                  ViewBag.isnew = true;
              }
                  return View(SelectedLayer);
         
        }
          public ActionResult Generic()
          {
              generic.Generic(repository.GetLayers(), Url.Action("Index", "WMS", null, Request.Url.Scheme, null)).Save(AppDomain.CurrentDomain.BaseDirectory + "wms.xml");
              return RedirectToAction("Index");
          }
          public ActionResult Update(WMSLayer l,string LayerName)
          {

              if (ModelState.IsValid)
                  repository.UpdateLayer(l,LayerName);
              else
                  TempData["ModelState"] = ViewData;
              l.Name = LayerName;
              return RedirectToAction("Index", new {NameLayer= l.Name });
          }
        public ActionResult Index(string NameLayer)
        {
            if (TempData["ModelState"]!=null)
            ViewData = (ViewDataDictionary)TempData["ModelState"];
            WMSLayer selectedLayer = repository.GetLayer(NameLayer);
          List<WMSLayer> list=repository.GetLayers();
          if ((selectedLayer != null) && (list.Count > 0))
          {
              ViewBag.SelectedLayer = selectedLayer;
              ViewBag.SelectedLayerName = selectedLayer.Name;
          }
           return View(list);
        }
      
        public ActionResult insertnew(WMSLayer l)
        {
            if (ModelState.IsValid)
                repository.AddLayer(l);
            else 
                    TempData["ModelState"] = ViewData;
        return    RedirectToAction("Index");
        }
    }
}
