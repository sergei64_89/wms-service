﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS_service.Models;
using WMSBDlibrary;

namespace WMS_service.Controllers
{
    public class GenericsController : Controller
    {
        //
        // GET: /Layers/
        IRepository repository;
        public GenericsController(IRepository repository)
        {
            this.repository = repository;
        }
        public ActionResult Detail(string Name)
        {
            return View(repository.GetLayer(Name));
        }

    }
}
