﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using WMSBDlibrary;

namespace WMS_service.Controllers
{
    class mashtab_class
    {
        public double mashtab;

        public double count;

    }
    public class WMSController : Controller
    {
        //
        // GET: /WMS/
        IRepository repository;
        public WMSController(IRepository repository)
        {
            this.repository = repository;
        }
      

        
 #region
 private  void _1XY(PointF p, PointF p1, int width, int height, double mashtab, string sloi,  Graphics graph, Bitmap ret)
 {

     double count_shirina = 0;

     //получаем большую картинку(скрепляем тайлы
     Bitmap main = new Bitmap(3000, 3000);


     //  double h = p.X - mashtab * 0.0682;
   //  double h2 = p.Y - mashtab * 0.0682;

     List<Tailer> list = repository.GetImage(p.X - mashtab, p.Y - mashtab, p1.X, p1.Y, mashtab, sloi);
  list=   list.OrderByDescending(h1 => h1.y).ThenBy(h2 => h2.x).ToList();
     count_shirina = p1.X + 1 - p.X;


     Graphics g = Graphics.FromImage(main);
     int po_x = 0, po_y = 0;
     count_shirina = -1;
     PointF tochka_otscheta = new PointF();
     foreach(Tailer t in list)
     {
        



         double hh = t.y;

         if (count_shirina != t.y)
         {

             if (count_shirina == -1)
             {
                 count_shirina = t.y;
                 tochka_otscheta.Y = (float)(t.y + mashtab);
                 tochka_otscheta.X = (float)(t.x);

             }
             else
             {
                 count_shirina = Convert.ToDouble(t.y);
                 po_y += 256;
                 po_x = 0;

             }
         }
        

         g.DrawImage(new Bitmap(t.path), new Point(po_x, po_y));
         po_x += 256;

     }
 

     //проводим обрезку картинки согласно запросу wms клиента



     //определим точки для вырезки
     //точки полученного изображения,которое требует обрезки
     double shag_taila = mashtab;

     p.Y = Math.Abs(tochka_otscheta.Y - p.Y);
     p1.Y = Math.Abs(tochka_otscheta.Y - p1.Y);
     p.X = Math.Abs(p.X - tochka_otscheta.X);
     p1.X = Math.Abs(p1.X - tochka_otscheta.X);


     p.X = (float)(p.X / shag_taila * 256);
     p1.X = (float)(p1.X / shag_taila * 256);
     p.Y = (float)(p.Y / shag_taila * 256);
     p1.Y = (float)(p1.Y / shag_taila * 256);
     PointF tochka = new PointF(p.X, p1.Y);

     graph.DrawImage(main, new RectangleF(0, 0, ret.Width, ret.Height), new RectangleF(tochka, new SizeF(Math.Abs(p1.X - p.X), Math.Abs(p.Y - p1.Y))), GraphicsUnit.Pixel);



 }
 //добавить слой на картинку
 private  void _1YX(PointF p, PointF p1, int width, int height, double mashtab, string sloi,  Graphics graph, Bitmap ret)
 {

     double count_shirina = 0;

     //получаем большую картинку(скрепляем тайлы
     Bitmap main = new Bitmap(2000, 2000);

     List<Tailer> list = repository.GetImage(p.X - mashtab, p.Y - mashtab, p1.X,p1.Y, mashtab, sloi);
     list=list.OrderByDescending(h1 => h1.x).ThenBy(h2 => h2.y).ToList();
     //  double h = p.X - mashtab * 0.0682;
   
     count_shirina = p1.X + 1 - p.X;


     Graphics g = Graphics.FromImage(main);
     int po_x = 0, po_y = 0;
     count_shirina = -1;
     PointF tochka_otscheta = new PointF();
     foreach (Tailer t in list)
     {
         



         double hh = t.x;

         if (count_shirina != t.x)
         {

             if (count_shirina == -1)
             {
                 count_shirina = Convert.ToDouble(t.x);
                 tochka_otscheta.Y = (float)(t.y);
                 tochka_otscheta.X = (float)(Convert.ToDouble(t.x) + mashtab);

             }
             else
             {
                 count_shirina = Convert.ToDouble(t.x);
                 po_y += 256;
                 po_x = 0;

             }
         }
       
         g.DrawImage(new Bitmap(t.path), new Point(po_x, po_y));
         po_x += 256;

     }
   
     //проводим обрезку картинки согласно запросу wms клиента



     //определим точки для вырезки
     //точки полученного изображения,которое требует обрезки
     double shag_taila = mashtab;

     p.Y = Math.Abs(tochka_otscheta.Y - p.Y);
     p1.Y = Math.Abs(tochka_otscheta.Y - p1.Y);
     p.X = Math.Abs(p.X - tochka_otscheta.X);
     p1.X = Math.Abs(p1.X - tochka_otscheta.X);


     p.X = (float)(p.X / shag_taila * 256);
     p1.X = (float)(p1.X / shag_taila * 256);
     p.Y = (float)(p.Y / shag_taila * 256);
     p1.Y = (float)(p1.Y / shag_taila * 256);
     PointF tochka = new PointF(p.Y, p1.X);

     graph.DrawImage(main, new RectangleF(0, 0, ret.Width, ret.Height), new RectangleF(tochka, new SizeF(Math.Abs(p.Y - p1.Y), Math.Abs(p1.X - p.X))), GraphicsUnit.Pixel);



 }
 public static double GetMashtab(List<double> mashtabs, PointF begin, PointF end, double width, double heigth)
 {
     double shag_taila;
     double width_h, height_h;
     mashtab_class mm = new mashtab_class();
     mm.mashtab = -1;
     width = width / 256;
     heigth = heigth / 256;
     double help;
     double must_have = width * heigth;
     width_h = end.X - begin.X;
     height_h = end.Y - begin.Y;
     foreach (double h in mashtabs)
     {





         shag_taila = h;


         //сколько
         help = (width_h / shag_taila) * (height_h / shag_taila);
         if (mm.mashtab == -1)
         {
             mm.count = help;
             mm.mashtab = h;
         }
         else
             if ((Math.Abs(mm.count - must_have) > Math.Abs(must_have - help)) && (help >= 0))
             {
                 mm.count = help;
                 mm.mashtab = h;
             }


     }
     return mm.mashtab;
 }

 #endregion

 private string GetCapabilities()
 {
     
     XDocument doc = new XDocument();
     doc = XDocument.Load(HttpContext.Server.MapPath("wms.xml"));
   return  doc.ToString();
 }
 public ActionResult Index(string request,List<string> LAYERS,string bbox, int WIDTH=0, int HEIGHT=0)
        {
            GC.Collect();
            switch (request)
            {
                case "GetCapabilities":
                    return this.Content(GetCapabilities(), "text/xml");
                    break;
                case "GetMap":
                    string help1;
                 string ss = bbox;
                    PointF p=new PointF();
            PointF p1=new PointF() ;
                   //получить координату x
               help1= ss.Substring( 0,ss.IndexOf(","));
               p.X =(float)Convert.ToDouble(help1.Replace('.',','));
               ss = ss.Substring(ss.IndexOf(",") + 1, ss.Length-1 - ss.IndexOf(","));
               //получить координату y
               help1 = ss.Substring(0, ss.IndexOf(","));
               p.Y = (float)(Convert.ToDouble(help1.Replace('.', ',')));
               ss = ss.Substring(ss.IndexOf(",") + 1, ss.Length-1 -  ss.IndexOf(","));

               help1 = ss.Substring(0, ss.IndexOf(","));

               p1.X = (float)(Convert.ToDouble(help1.Replace('.', ',')) );
               ss = ss.Substring(ss.IndexOf(",") + 1, ss.Length - 1 - ss.IndexOf(","));

                       help1 = ss;
               p1.Y = (float)(Convert.ToDouble(help1.Replace('.', ',')) ) ; 
                    double mashtab=GetMashtab(repository.GetMashtab().Select(n=>n.Name).ToList(),p,p1,WIDTH,HEIGHT);
                    Bitmap help = new Bitmap(3000, 3000);

                    Bitmap main = new Bitmap(WIDTH, HEIGHT);
            Graphics main_graphics = Graphics.FromImage(main);
                    List<WMSLayer> listlayer = repository.GetLayers();
                        WMSLayer hlayer;
                    foreach (string layer in LAYERS)
                    {
                        hlayer=listlayer.Where(n=>n.Name==layer).FirstOrDefault();
                        if (hlayer.MashtabWMSLayers.Where(n=>n.Mashtab.Name==mashtab).Count()==1)
                            if (hlayer.Osi == OSI._1YX)
                                _1YX(p, p1, WIDTH, HEIGHT, mashtab, layer, main_graphics, main);
                                else
                                if (hlayer.Osi == OSI._1XY)
                                    _1XY(p, p1, WIDTH, HEIGHT, mashtab, layer, main_graphics, main);
                            
                    }
                    main.Save(@"C:\1.jpg");
                     MemoryStream stream = new MemoryStream();
            main.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            return new  StreamActionResult(stream);
                    break;
            }
            return View();
        }

    }
}
