﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WMS_service
{
    public class StreamActionResult : System.Web.Mvc.ActionResult
    {

        public StreamActionResult(MemoryStream memory)
        {
            this.stream = memory;
        }
        MemoryStream stream;
      
        public override void ExecuteResult(ControllerContext context)
        {
           
                context.HttpContext.Response.ContentType = "image/png";
                byte[] buffer = stream.ToArray();
                context.HttpContext.Response.OutputStream.Write(buffer, 0, buffer.Length);
            



        }
    }
}