﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using WMS_service.Models;
using WMSBDlibrary;

namespace WMS_service.NinjectFactory
{
    
    public class NinjectControllFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public NinjectControllFactory()
        {
            ninjectKernel = new StandardKernel();
           
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);

        }
        private void AddBindings()
        {

            ninjectKernel.Bind<IRepository>().To<EFRepository>();
            ninjectKernel.Bind<IGenericXmlWms>().To<Get_xml>();
        }
    }
}