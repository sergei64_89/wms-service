﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace WMSBDlibrary
{
   
       public class ProjectBD 
       {
           private IKernel ninjectKernel;
           public ProjectBD()
           {
               ninjectKernel = new StandardKernel();
               Bind();
           }
           public IRepository GetRepository()
           {
              return (IRepository) ninjectKernel.Get(typeof(IRepository));
           }
           public IGenericXmlWms GetGenericXmlWms()
           {
               return (IGenericXmlWms)ninjectKernel.Get(typeof(IGenericXmlWms));
           }
           private void Bind()
           {
               ninjectKernel.Bind<IRepository>().To<EFRepository>();
               ninjectKernel.Bind<IGenericXmlWms>().To<Get_xml>();
               
           }
       }
        
}
