﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace WMSBDlibrary
{
  public  class EFRepository:IRepository
    {
      private EF context = new EF();
      //добавить тайл
      public bool AddTailToLayer(string nameLayer, string path, double mashtab, double x, double y)
      {
          TransactionOptions options = new TransactionOptions
          {
              IsolationLevel = IsolationLevel.ReadCommitted
          };
          //Проверить есть ли в бд такой слой и маштаб-добавить в случае отсутствия
          MashtabWMSLayer ML;
          using (var scope = new TransactionScope(TransactionScopeOption.Required, options))
          {
              Mashtab masht = context.Mashtabs.Where(n => n.Name == mashtab).FirstOrDefault();
              WMSLayer Layer = context.Layers.Where(n => n.Name == nameLayer).FirstOrDefault();
              if (masht == null)
                  context.Mashtabs.Add(new Mashtab { Name = mashtab });
              if (Layer == null)
                  context.Layers.Add(new WMSLayer { Name = nameLayer, GeogBox = new GeographicBoundingBox(), Box = new BoundingBox() });
              try
              {
                  context.SaveChanges();
              }
              catch
              {
                  foreach (DbEntityEntry entry in context.ChangeTracker.Entries())
                  {
                      switch (entry.State)
                      {
                           
                          case EntityState.Modified:
                              entry.State = EntityState.Unchanged;
                              break;
                          case EntityState.Added:
                              entry.State = EntityState.Detached;
                              break;
                         
                          case EntityState.Deleted:
                              entry.Reload();
                              break;
                          default: break;
                      }
                  }
              }
               ML = context.MashtabLayer.Where(n => n.Mashtab.Name == mashtab && n.WMSLayer.Name == nameLayer).FirstOrDefault();
              if (ML == null)
              {
                  context.MashtabLayer.Add(new MashtabWMSLayer
                  {
                      Mashtab = context.Mashtabs.Where(n => n.Name == mashtab).FirstOrDefault(),
                      WMSLayer = context.Layers.Where(h => h.Name == nameLayer).FirstOrDefault()
                  });
                  context.SaveChanges();
                  ML = context.MashtabLayer.Where(n => n.Mashtab.Name == mashtab && n.WMSLayer.Name == nameLayer).FirstOrDefault();
              }
              scope.Complete();
          }
          
              Tailer tail = ML.Tailers.Where(n => n.x == x && n.y == y).FirstOrDefault();
              if (tail == null)
              {
                  ML.Tailers.Add(new Tailer
                  {
                      path = path,
                      x = x,
                      y = y
                  });
              }
              else
              {
                  tail.path = path;
                  tail.x = x;
                  tail.y = y;
              }

             
                  context.SaveChanges();
             
            
          
          return true;
  }
      //добавить слой
      public bool AddLayer(WMSLayer layer)
      {
          context.Layers.Add(layer);
          context.SaveChanges();
          return true;
      }
      //удалить слой
      public bool RemoveLayer(string layer)
      {
          context.Layers.Remove(context.Layers.Where(k=>k.Name==layer).FirstOrDefault());
          context.SaveChanges();
          return true;
      }

      //обновить слой
      public bool UpdateLayer(WMSLayer layer,string LayerName)
      {
          var l = context.Layers.Where(n => n.Name == LayerName).FirstOrDefault();
          if (l != null)
          {
              l.Name = layer.Name;
              l.CRS = layer.CRS;
              l.GeogBox = layer.GeogBox;
              l.Box = layer.Box;
          }
          context.SaveChanges();
          return true;
      }
      public List<WMSLayer> GetLayers()
      {
          return context.Layers.ToList();
      }
      public WMSLayer GetLayer(string name)
      {
          return context.Layers.Where(n=>n.Name==name).FirstOrDefault();
      }

      public List<Mashtab> GetMashtab()
      {
        return  context.Mashtabs.ToList();
      }

      public List<Tailer> GetImage(double minx,double miny,double maxx,double maxy, double mashtab, string layer)
      {
          return context.MashtabLayer.Where(n => n.WMSLayer.Name == layer && n.Mashtab.Name == mashtab).FirstOrDefault().Tailers.Where(f => f.x >= minx && f.y >= miny && f.x <= maxx && f.y <= maxy).ToList();
      }
     
    }
}
