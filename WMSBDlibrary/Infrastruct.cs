﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace WMSBDlibrary
{
    #region конфигурационные файлы
    public enum OSI
    {
        _1XY = 0,
        _1YX = 1
    }
    public class GeographicBoundingBox
    {
        public string westBoundLongitude { get; set; }
        public string eastBoundLongitude { get; set; }
        public string southBoundLatitude { get; set; }
        public string northBoundLatitude { get; set; }
    }
    public class BoundingBox
    {
        public string minx { get; set; }
        public string miny { get; set; }
        public string maxx { get; set; }
        public string maxy { get; set; }
    }
   
    public partial class WMSLayer
    {

        public WMSLayer()
        {
            this.MashtabWMSLayers = new HashSet<MashtabWMSLayer>();
        }
        [Key]
        public string Name { get; set; }
        public string CRS { get; set; }
        public GeographicBoundingBox GeogBox { get; set; }
        public BoundingBox Box { get; set; }
        public OSI Osi { get; set; }

        public virtual ICollection<MashtabWMSLayer> MashtabWMSLayers { get; set; }
    }

    public class Tailer
    {
        public int TailerID { get; set; }
        public string path { get; set; }
        public double x { get; set; }
        public double y { get; set; }


        public virtual MashtabWMSLayer MashtabWMSLayer { get; set; }
    }
    public class Mashtab
    {
        public Mashtab()
        {
            this.MashtabWMSLayers = new HashSet<MashtabWMSLayer>();
        }
        [Key]
        public double Name { get; set; }

        public virtual ICollection<MashtabWMSLayer> MashtabWMSLayers { get; set; }
    }
    public class MashtabWMSLayer
    {
        public MashtabWMSLayer()
        {
           this.Tailers = new HashSet<Tailer>();
        }


        public int id { get; set; }

        public virtual Mashtab Mashtab { get; set; }
        public virtual WMSLayer WMSLayer { get; set; }
        public virtual ICollection<Tailer> Tailers { get; set; }
    }
    #endregion
    //интерфейс для генерации xml файла-конфигурации wms
    public interface IGenericXmlWms
    {
        XDocument Generic(List<WMSLayer> list,string url);
    }

    public class Project
    {

    }

}