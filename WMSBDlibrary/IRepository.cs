﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMSBDlibrary
{
    public interface IRepository
    {   /// <summary>
        /// Загрузить описание слоёв wms
        /// </summary>

        bool AddTailToLayer(string nameLayer, string path, double mashtab, double x, double y);
        bool AddLayer(WMSLayer layer);
        bool RemoveLayer(string layer);
        bool UpdateLayer(WMSLayer layer, string LayerName);
        List<WMSLayer> GetLayers();
        WMSLayer GetLayer(string name);
        List<Mashtab> GetMashtab();
        List<Tailer> GetImage(double minx , double miny,double maxx,double maxy, double mashtab, string layer);
    }
}
