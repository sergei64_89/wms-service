﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Linq;
using System.Collections.ObjectModel;
namespace WMSBDlibrary
{
    public  class Get_xml : IGenericXmlWms
    {

        public XDocument Generic(List<WMSLayer> list_layer, string url)
        {
            XNamespace xmlns = "http://www.opengis.net/wms";
            XNamespace xlink = "http://www.w3.org/1999/xlink";
            XDocument doc = new XDocument(new XDeclaration("1.0", "windows-1251", "yes"));
            XElement WMS_Capabilities = new XElement(xmlns + "WMS_Capabilities",
                new XAttribute(XNamespace.Xmlns + "xlink", xlink.NamespaceName),
                new XAttribute("xmlns", "http://www.opengis.net/wms"), new XAttribute("version", "1.3.0"));
            doc.Add(WMS_Capabilities);
            XElement Service = new XElement(xmlns + "Service");

            XElement Name = new XElement(xmlns + "Name");
            Name.Value = "WMS";
            Service.Add(Name);
            XElement Title = new XElement(xmlns + "Title");
            Title.Value = "WMS 1.0";
            Service.Add(Title);
            WMS_Capabilities.Add(Service);
            XElement Capability = new XElement(xmlns + "Capability");
            WMS_Capabilities.Add(Capability);
            XElement Request = new XElement(xmlns + "Request");
            Capability.Add(Request);
            List<string> list = new List<string>();
            XElement GetCapabilities = new XElement(xmlns + "GetCapabilities");
            list.Add("application/vnd.ogc.wms_xml");
            list.Add("text/xml");
            add_Format(GetCapabilities, list, xmlns);

            add_DCPType(GetCapabilities, xmlns, xlink, url);
            XElement GetMap = new XElement(xmlns + "GetMap");
            Request.Add(GetMap);
            list.Clear();
            list.Add("image/bmp");
            list.Add("image/jpeg");
            list.Add("image/png");

            add_Format(GetMap, list, xmlns);
            XElement DCPType = new XElement(xmlns + "DCPType");
            GetMap.Add(DCPType);
            add_DCPType(DCPType, xmlns, xlink, url);
            XElement GetFeatureInfo = new XElement(xmlns + "GetFeatureInfo");
            Request.Add("GetFeatureInfo");
            list.Clear();
            list.Add("text/xml");
            add_Format(GetFeatureInfo, list, xmlns);
            add_DCPType(GetFeatureInfo, xmlns, xlink, url);

            //Insert_CRS(element_layer, pro);
            foreach (WMSLayer l in list_layer)
                Insert_layer(Capability, l);
            return doc;
        }

        private void add_Format(XElement parent, List<string> text, XNamespace xmlns)
        {
            XElement Format;
            foreach (string s in text)
            {
                Format = new XElement(xmlns + "Format", s);
                parent.Add(Format);
            }
        }
        private void add_DCPType(XElement parent, XNamespace xmlns, XNamespace xlink, string url)
        {
            XElement HTTP = new XElement(xmlns + "HTTP", new XElement(xmlns + "Get", new XElement(xmlns + "OnlineResource",
                new XAttribute(XNamespace.Xmlns + "xlink", "http://www.w3.org/1999/xlink"), new XAttribute(xlink + "type", "simple"),
                new XAttribute(xlink + "href", url)
                )



                ));
            parent.Add(HTTP);
        }
        public void Insert_layer(XElement elem, WMSLayer l)
        {
            XElement element = new XElement(elem.GetDefaultNamespace() + "Layer");

            XElement second = new XElement(elem.GetDefaultNamespace() + "Name", l.Name);
            XElement EX_GeographicBoundingBox = new XElement(elem.GetDefaultNamespace() + "EX_GeographicBoundingBox");
            element.Add(EX_GeographicBoundingBox);
            element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "Title", l.Name);
            element.Add(second);

            //  second = new XElement(elem.GetDefaultNamespace() + "MaxScaleDenominator", l.mashtabi.);
            //  element.Add(second);
            //  second = new XElement(elem.GetDefaultNamespace() + "MinScaleDenominator", l.min_mashtab);
            // element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "BoundingBox", new XAttribute("CRS", l.CRS), new XAttribute("minx", l.Box.minx),
                new XAttribute("miny", l.Box.miny), new XAttribute("maxx", l.Box.maxx), new XAttribute("maxy", l.Box.maxy));
            element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "CRS", l.CRS);
            element.Add(second);
            //EX_GeographicBoundingBox
            second = new XElement(elem.GetDefaultNamespace() + "westBoundLongitude", l.GeogBox.westBoundLongitude);
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "eastBoundLongitude", l.GeogBox.eastBoundLongitude);
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "southBoundLatitude", l.GeogBox.southBoundLatitude);
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "northBoundLatitude", l.GeogBox.northBoundLatitude);
            EX_GeographicBoundingBox.Add(second);

            elem.Add(element, XNamespace.None);
        }
        public void Insert_CRS(XElement elem, List<WMSLayer> list)
        {
            IEnumerable<string> ll;
            XElement new_element = new XElement(elem.GetDefaultNamespace() + "CRS");
            XElement new_element2;
            elem.Add(new_element);

            List<string> list_ = new List<string>();
            foreach (WMSLayer l in list)
            {
                list_.Add(l.CRS);
            }
            ll = list_.Distinct();
            foreach (string s in ll)
            {
                new_element2 = new XElement(elem.GetDefaultNamespace() + "CRS", s);
                new_element.Add(new_element2);
            }
            elem.Add(new_element);
        }
    }



}
