﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMSBDlibrary
{
    public class EF : DbContext
    {
        public DbSet<WMSLayer> Layers { get; set; }
        public DbSet<Tailer> Tailers { get; set; }
        public DbSet<MashtabWMSLayer> MashtabLayer { get; set; }
        public DbSet<Mashtab> Mashtabs { get; set; }
        public EF()
        {
            this.Database.Connection.ConnectionString = @"Data Source=SERGEI\SQLEXPRESS2012;Initial Catalog=temp28;Integrated Security=True" ;

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<WMSLayer>().HasMany(t=>t.MashtabWMSLayers).WithRequired(l=>l.WMSLayer)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Mashtab>().HasMany(t => t.MashtabWMSLayers).WithRequired(l => l.Mashtab)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<MashtabWMSLayer>().HasMany(t => t.Tailers).WithRequired(l => l.MashtabWMSLayer)
                .WillCascadeOnDelete(true);
        }
    }
}
