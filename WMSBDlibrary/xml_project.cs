﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Collections.ObjectModel;
namespace WpfApplication4
{
    public class Get_xml
    {
        public Get_xml(string path,string output_path,project pro,string url)
        {
            this.path = path;
            this.output_path = output_path;
        this.pro=pro;
        this.url = url;
        }
        public void Go()
        {
            XNamespace xmlns = "http://www.opengis.net/wms";
               XNamespace xlink="http://www.w3.org/1999/xlink";
            XDocument doc = new XDocument(new XDeclaration("1.0", "windows-1251", "yes"));
            XElement WMS_Capabilities = new XElement(xmlns+"WMS_Capabilities",
                new XAttribute(XNamespace.Xmlns+"xlink",xlink.NamespaceName),
                new XAttribute("xmlns","http://www.opengis.net/wms"), new XAttribute("version", "1.3.0"));
            doc.Add(WMS_Capabilities);
            XElement Service = new XElement(xmlns + "Service");

            XElement Name = new XElement(xmlns + "Name");
            Name.Value = "WMS";
            Service.Add(Name);
            XElement Title = new XElement(xmlns + "Title");
            Title.Value = pro.NAME;
            Service.Add(Title);
            WMS_Capabilities.Add(Service);
            XElement Capability = new XElement(xmlns + "Capability");
            WMS_Capabilities.Add(Capability);
            XElement Request = new XElement(xmlns + "Request");
            Capability.Add(Request);
            List<string> list = new List<string>();
            XElement GetCapabilities = new XElement(xmlns+"GetCapabilities");
            list.Add("application/vnd.ogc.wms_xml");
            list.Add("text/xml");
            add_Format(GetCapabilities, list,xmlns);

            add_DCPType(GetCapabilities, xmlns, xlink, url);
            XElement GetMap = new XElement(xmlns + "GetMap");
            Request.Add(GetMap);
            list.Clear();
            list.Add("image/bmp");
            list.Add("image/jpeg");
            list.Add("image/png");
       
            add_Format(GetMap, list,xmlns);
            XElement DCPType = new XElement(xmlns + "DCPType");
            GetMap.Add(DCPType);
            add_DCPType(DCPType, xmlns, xlink, url);
            XElement GetFeatureInfo = new XElement(xmlns + "GetFeatureInfo");
            Request.Add("GetFeatureInfo");
            list.Clear();
            list.Add("text/xml");
            add_Format(GetFeatureInfo, list,xmlns);
            add_DCPType(GetFeatureInfo, xmlns, xlink, url);
           
            //Insert_CRS(element_layer, pro);
            foreach (Layer l in pro.list)
            Insert_layer(Capability, l);
            doc.Save(output_path+"\\wms.xml");
        }
        string path;
        string output_path;
        project pro;
        string url;
        private void add_Format(XElement parent,List<string> text,XNamespace xmlns)
        {
            XElement Format;
            foreach (string s in text)
            {
                Format = new XElement(xmlns+"Format",s);
                parent.Add(Format);
            }
        }
        private void add_DCPType(XElement parent,XNamespace xmlns,XNamespace xlink,string url)
        {
            XElement HTTP = new XElement(xmlns + "HTTP", new XElement(xmlns + "Get", new XElement(xmlns+"OnlineResource",
                new XAttribute(XNamespace.Xmlns+"xlink","http://www.w3.org/1999/xlink"),new XAttribute(xlink+"type","simple"),
                new XAttribute(xlink+"href",url)
                )
                
             
                
                ));
            parent.Add(HTTP);
        }
        public void Insert_layer(XElement elem,Layer l)
        {
            XElement element = new XElement(elem.GetDefaultNamespace()+"Layer");
      
            XElement second=new XElement(elem.GetDefaultNamespace()+"Name",l.layer);
           XElement EX_GeographicBoundingBox=new XElement(elem.GetDefaultNamespace()+"EX_GeographicBoundingBox");
            element.Add(EX_GeographicBoundingBox);
            element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "Title", l.layer);
            element.Add(second);

          //  second = new XElement(elem.GetDefaultNamespace() + "MaxScaleDenominator", l.mashtabi.);
          //  element.Add(second);
          //  second = new XElement(elem.GetDefaultNamespace() + "MinScaleDenominator", l.min_mashtab);
           // element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "BoundingBox", new XAttribute("CRS", l.System_coordinate), new XAttribute("minx", l.minx),
                new XAttribute("miny",l.miny),new XAttribute("maxx",l.maxx),new XAttribute("maxy",l.maxy));
            element.Add(second);
            second = new XElement(elem.GetDefaultNamespace() + "CRS", l.System_coordinate);
            element.Add(second);
            //EX_GeographicBoundingBox
            second = new XElement(elem.GetDefaultNamespace() + "westBoundLongitude", l.westBoundLongitude.ToString());
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "eastBoundLongitude", l.eastBoundLongitude.ToString());
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "southBoundLatitude", l.southBoundLatitude.ToString());
            EX_GeographicBoundingBox.Add(second);

            second = new XElement(elem.GetDefaultNamespace() + "northBoundLatitude", l.northBoundLatitude.ToString());
            EX_GeographicBoundingBox.Add(second);

            elem.Add(element,XNamespace.None);
        }
        public void Insert_CRS(XElement elem, project proj)
        {
            IEnumerable<string> ll;
            XElement new_element = new XElement(elem.GetDefaultNamespace() + "CRS");
            XElement new_element2;
            elem.Add(new_element);

            List<string> list_=new List<string>();
            foreach (Layer l in proj.list)
            {
                list_.Add(l.System_coordinate);
            }
           ll=list_.Distinct();
           foreach (string s in ll)
           {
               new_element2 = new XElement(elem.GetDefaultNamespace() + "CRS", s);
               new_element.Add(new_element2);
           }
           elem.Add(new_element);
        }
    }


   public static class xml_project
    {




       //формируем документ
       public static void Begin_proc(out XDocument doc, ObservableCollection<Layer> sp_layer, string connection_bd)
       {
           List<string> list_mashtabs = new List<string>();
       
           XElement hh;
          doc = new XDocument(new XDeclaration("1.0","utf-8","yes"));
           XElement hhh=new XElement("Project");
        XElement   elem=new XElement("Layers");
           doc.Add(hhh);
           hhh.Add(elem);
           foreach (Layer l in sp_layer)
           {
              
               Add_layer(elem, l.layer, out hh);
                add_epgs(hh,l.System_coordinate);
                add_osi(hh, l.osi);
              foreach (int current in l.mashtabi)
              {
                   list_mashtabs.Add(current.ToString());
                   Add_mashtab_to_layer(hh, current.ToString());
               }
           }
           //подключение к бд
           hhh.Add(new XElement("ConnectionString", connection_bd));
           hh = new XElement("Mashtabs_project");
           hhh.Add(hh);
           IEnumerable<string> gg = list_mashtabs.Distinct();
           XElement hh_help;
           foreach (string mash in gg)
           {
              hh_help = new XElement("mashtab", mash);
               hh.Add(hh_help);
           }
          
       }
       private static void add_epgs(XElement layer, string epgs)
       {
           XElement ep = new XElement("epgs");
           ep.Value = epgs;
           layer.Add(ep);
       }
       //добавляем расположение осей
       private static void add_osi(XElement layer, string osi)
       {
           XElement elem_osi = new XElement("osi");
           elem_osi.Value = osi;
           layer.Add(elem_osi);
       }
       //вставляем один слой
       public static void Add_layer(XElement layers,string name,out XElement layer)
       {
           layer = new XElement("layer", new XAttribute("name", name),new XElement("mashtabs"));
           
           layers.Add(layer);
       }
       //вставляем обработанный маштаб к слою
       public static void Add_mashtab_to_layer(XElement layer,string mashtab)
       {
           XElement elem_mashtab=new XElement("mashtab",mashtab);
           layer.Element("mashtabs").Add(elem_mashtab);
           }
       public static bool SaveDoc(XDocument doc,string path)
       {
           try
           {
               doc.Save(path+"//list.xml");
           }
           catch
           {
               return false;
           }
           return true;
       }
    }
}
